-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2018 at 06:07 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `company`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `id` int(10) NOT NULL,
  `CompanyName` varchar(225) NOT NULL,
  `Phone Number` varchar(225) NOT NULL,
  `Address` varchar(225) NOT NULL,
  `Website` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `CompanyName`, `Phone Number`, `Address`, `Website`) VALUES
(1, 'Peninsula Enterprises Ltd', '(02)36720', 'No.134,26th(B)st,Bet 83rd & 84th St,Aung Myae Thar Zan, Mandalay', 'www.peninsulaasia.com'),
(2, 'Myanmar Kaido Co., Ltd.', '(02) 70330', 'No.Nga-7,Yangon-Mandalay Rd, Mandalay', 'www.myanmarkaido.com'),
(3, 'Min Htet Thar', '(02) 092055435', 'Nya Nya/46, No.39, Bet 54th St & 55th St,Myo Thit ,Mandalay', 'www.minhtetthar.com'),
(4, 'May & Mark Gems', '(02) 33229', '12/5, 30th St, Bet 73rd & 74th St,Chan Aye Thar San, Mandalay', 'www.maynmark.com'),
(5, 'Kyaw Zeyar Telecommunications Services Co.,Ltd.', '(02) 61621', 'Top Flr,Cor of 39th & 73rd St,Mingalar Market, Madalay', 'www.panasonicmyanmar.com'),
(6, 'Kedia Lights', '(02) 36242', 'No.A-6/7/8/9, 78th Rd,Bet 31st & 32nd St,Below Fantasy World,Mandalay', 'www.acculighting.com'),
(7, 'Kan Htoo Kan Thar', '(02) 64207', '163, 80th St,Bet 28th & 28th St,Mandalay', 'www.kanhtookanthar.com'),
(8, 'Hotel Royal', '(02) 24676', '80th St, Bet 27th & 28th St,Mandalay', 'www.royalhotel.com'),
(9, 'Hotel Mandalay', '(02) 71584', 'No.652 , 78th St,Bet 37th & 38th St,Mandalay', 'www.hotelmdy.com'),
(10, 'Hongta Hotel', '(02) 36274', '176,82nd St, Bet 25th & 26th St,Aung Myay Thar Zan,Mandalay', 'www.hongtahotel.com'),
(11, 'Heritage Sports & Golf', '(02) 34918', 'H-1, 30th St,Bet 77th & 78th St,Mandalay', 'www.heritagegolfing.com'),
(12, 'Hein Engineering Group', '(02) 34824', 'H-198 A,Ka Naung Min Thar Kyi St,Mandalay', 'www.heinengineering.com'),
(13, 'Gusto', '(02) 0991000722', '2nd Flr, Bet 32nd St & 33rd St,Shwe Phyu Plaza,Mandalay', 'www.gustomyanmar.com'),
(14, 'Golden Country', '(02) 72002', '65th St, Bet 31st & 32nd St,Mandalay', 'www.goldencountry.com'),
(15, 'Golden Power Technology', '(02) 098621013', 'A-2, 78th Rd,Bet 27th & 28th St,Mandalay', 'www.goldenpower.com'),
(16, 'Golden Triangle', '(02) 31421', '85, 84th St,Bet 28th & 29th St,Mandalay', 'www.gthtrading.com'),
(17, 'Golden Key', '(02) 092012481', 'No.B-7/8, Bet 77th & 78th St,Mandalay', 'www.mikkofood.com'),
(18, 'Fuji', '(02) 35429', 'No.427, 82nd Rd,Bet 32nd & 33rd St,Chan Aye Thar San,Mandalay', 'www.fujimayanmar.com'),
(19, 'Diamond Rose Co.,Ltd.', '(02) 68674', 'No.122,73rd St,Bet 29th & 30th St,Mandalay', 'www.trinricosteed.com'),
(20, 'Champion', '(02) 092006970', 'No.6,26th St,Bet 86th & 87th St,Aung Myay Thar San,Mandalay', 'www.luckychampion.com'),
(21, 'ADK Advertising Group', '(02) 092039016', 'Plot-801,65th St,Bet 26th & 27th St,Mandalay', 'www.adkadvertising.com'),
(22, 'United Pacific Co., Ltd.', '(01) 652888', 'No.79, Oak Pon Seik St,Mayangone,Mandalay', 'www.unitedpacificmm.com'),
(23, 'Yadanabon', '(02) 31071', '61st St,Bet 41st & 42nd St,Mahar Myaing(2),Mandalay', 'www.yadanabon.com'),
(24, 'Sweety Life', '(02) 5153850', 'D-1/A, 62nd St,Pan Khinn St,Industrial Zone(1),Mandalay', 'www.maykaung.com'),
(25, 'Shining Strar Tours Express Ltd', '(02) 36335', 'No.279, 81st St,Bet 26th & 27th St, CATZ,Mandalay', 'www.shiningstartours.com'),
(26, 'Sein Sein Hotel', '(02) 32617', 'No.446,81st St,Bet 32nd & 33rd St,Mandalay', 'www.seinseinhotel.com'),
(27, 'Sedona Hotel', '(02) 36488', 'No.1,Junction of 26th St & 66th St,Chan Aye Thar Zan Tsp,Mandalay', 'www.sedonamyanmar.com'),
(28, 'One Nine Steel Trading Co.Ltd', '02-5154565', '28st bet 80*81,Mandalay', 'www.onenine.com'),
(29, 'Leo', '(02)092038747', '9/2,42nd St,Bet 62nd & 63rd St,Mandalay', 'www.minnaingkhant.com'),
(30, 'Ayar Min', '(02) 80187', 'Ma-9/11, 64th St,Bet Cherry St & Theikpan St,Mandalay', 'www.abgroup.com'),
(31, 'Eastern Photo Studio', '(02) 73277', '182,28th St,Bet 80th & 81st St,Mandalay', 'www.easternphoto.com'),
(32, 'Dream House', '(02) 24999', 'No.232,33rd St,Bet 81st & 82nd St,Mandalay', 'www.dreamhouse.com'),
(33, 'Central Computer', '(02) 0991004401', 'Rm.B-1, 40th St,Cor 73rd St,Mandalay', 'www.centralmdy.com'),
(34, 'Swe Tha Har Myanmar Co.,Ltd.', '(02) 23278', 'No.252/2, 82nd St,Bet 23rd St & 24th St,Mandalay', 'www.swethaharmm.com'),
(35, 'Mobile Mother', '02-72041', '26th St,76*77 St,Mandalay', 'www.mobilemotherinternational.com'),
(36, 'Mandalay Cement Industries Co Ltd', '(02) 44537', 'Patheigyi,Mandalay', 'www.mandalaycement.com'),
(37, '7 Copy', '(02) 0991001608', '34th Street,Between 72nd & 73rd Street,Mandalay', 'www.7copy.com'),
(38, 'Orient-Software Development', '0991037890', 'No.A5-A6,Bet 27th St & 28th St,iPlaza,2nd Floor of Man Myanmar Plaza.Near Zegyo.,Mandalay', 'www.orientitsolution.com'),
(39, 'GENIUS', '09793444401', '86th Street,Between 15th * 16th Street,Aung Myae Thar San,Mandalay', 'www.geniusmobilemdy.com'),
(40, 'Min Naing Khant Co.,Ltd', '09420039333', 'Na-9/2,42nd St,Bet 62nd & 63rd St.Mahar Myaing Qt(2),Mahar Aung Myay Township,Mandalay', 'www.minnaingkhant.com'),
(41, 'Polygold Co,.Ltd.', '(02) 39926', 'E/8,36th Street,Between 60th and 61st Street,Mandalay', 'www.polygoldmm.com'),
(42, 'Cyber Smart Computer', '(02) 38160', 'No.0-1,22nd St, Bet 86th & 87th St,Mandalay', 'www.cybersmart.com'),
(43, 'Cyber Friends', '(02) 72725', 'No.75,29th St,Bet 73rd & 74th St,Mandalay', 'www.cyberfriend.com'),
(44, 'Daiden Equipment Co., Ltd.', '(02) 5154543', '594,Plot-876/878, Yangon-Mandalay Rd,Tagon Taing Qtr,Mandalay', 'www.daidenequipment.com'),
(45, 'Dana Thukha', '(02) 095119464', 'No.102/24,Bet 38th St & 39th St,Mandalay', 'www.danathukha.com'),
(46, 'Diamond Palace', '(02) 092017009', 'Cor of 73rd & 28th St,Chan Aye Thar San Tsp,Mandalay', 'www.diamondpalace.com'),
(47, 'Digital World', '(02) 63377', 'No.544, 83rd Rd,Bet 37th & 38th St,Mandalay', 'www.digitalworld.com'),
(48, 'Dream Hotel', '(02) 60470', 'No.152,27th St,Bet 80th & 81st St,Mandalay', 'www.dreamhotel.com'),
(49, 'Eleven Seven', '(02) 72236', 'No.117,Aung Thu Kha St,Bet 68th & 69th St,Mahar Myaing(1),Mandalay', 'www.elevenseven.com'),
(50, 'Global Com', '(02) 63149', 'No.498,84th St,Bet 44nd & 45th St,Mandalay', 'www.globalcom.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
